# 高德地图百度地图纠偏

## 1、写在前面

在使用高德地图API和百度地图API的时候，如果要加载地图服务如WMS，WMTS等，这些地图服务常用的投影坐标系是EPSG:3857。加载上去会发现存在偏移，因为投影坐标系不一致。

高德的坐标系是GCJ-02，而百度的坐标系是在GCJ-02再次偏移的BD-09，这些坐标系是没有收录在EPSG中的，所以无法用Proj.4库来做坐标转换。

我们是否可以通过整体的偏移来做呢？不行的，因为GCJ-02坐标系相对于WMS坐标系的偏差是非线性随机的。这么做感觉就是在为难国内的开发者，一方面不能不使用WGS坐标，因为这个是国际通用的，另一方面又在设置重重障碍让WGS坐标和GCJ-02坐标难以转化。

不过也不是束手无策的，高德和百度都有提供单点的坐标转换功能，我们可以利用单点的坐标转换来实现切片的偏移。也有一个开源的项目 [gcoord](https://github.com/hujiulong/gcoord) 融合了百度高德的转化

## 2、思路

百度高德在请求切片图层的时候，对于每一个切片来说，切片的BBOX坐标是可以计算出来的。在默认情况下，会使用计算出来的BBOX坐标请求WMS或是WMTS服务，这样是有偏差的。我们可以对计算出来的BBOX坐标进行单点偏移，使用偏移后的BBOX坐标请求地图服务就可以实现地图的吻合。

## 3、问题

使用gcoord坐标转换时，高德没有什么问题，但是百度转换后出现切片错位的情况,出现这个问题的原因有点棘手。因为百度的坐标是在WGS84坐标的基础上做了两次偏移，直接将百度坐标转为WGS84的坐标是一个近似的结果，切片就会出现错位。

可以想到的一个方法是：
* 将WMS数据源的坐标系改为百度坐标系，整体做偏移。